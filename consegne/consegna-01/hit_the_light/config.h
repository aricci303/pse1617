/*
 *  This header file stores symbols that concerns 
 *  the configuration of the system.
 *
 */
#ifndef __CONFIG__
#define __CONFIG__

#define LED01_PIN 4
#define LED02_PIN 5
#define LED03_PIN 6

#define BUT01_PIN 7
#define BUT02_PIN 8
#define BUT03_PIN 9

#define PULSELED_PIN 3

#define POT_PIN A0

#define MAX_INITIAL_DELAY_USER_SEL 4000
#define MAX_MOVE_DURATION 3000
#define MIN_MOVE_DURATION 500


#endif

