#include "Arduino.h"

extern long score;
extern bool gameover;
extern int selectedLed;
extern int buttonPin[3];
extern int ledPin[3];
extern long maxDelayUserSelection;

static int getPressed(){
  for (int i = 0; i < 3; i++){
    if (digitalRead(buttonPin[i])==HIGH){
      return i+1;  
    }
  }  
  return 0;
}

void checkUserSelection(){
   digitalWrite(ledPin[selectedLed],HIGH);
   int pressed = getPressed();
   long currentTime = 0;  
   long t0 = millis(); 
   while (!pressed && currentTime < maxDelayUserSelection){
     delay(10);
     pressed = getPressed();
     long t1 = millis();
     currentTime = currentTime + (t1-t0);
     t0 = t1;
   }  
   digitalWrite(ledPin[selectedLed],LOW);
   if (pressed && (pressed-1) == selectedLed){
     score = score + (maxDelayUserSelection - currentTime);  
     maxDelayUserSelection = maxDelayUserSelection / 2;
   } else {
     gameover = true; 
   }
}


