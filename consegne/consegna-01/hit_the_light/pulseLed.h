/*
 * This module is responsible of the initial pulsing  
 * of the led and to detect the input to start a new
 * game 
 *
 */
#ifndef __PULSELED__
#define __PULSELED__

void pulseLed();
void flashLed();

#endif


