#include "Arduino.h"
#include "config.h"
#include "MoveLed.h"

extern int buttonPin[3];
extern int moveDelay;

void pulseLed(){
  int currIntensity = 0;
  int fadeAmount = 5;
  int pressed = digitalRead(buttonPin[0]);
  while (!pressed){
    double ledSpeed = analogRead(POT_PIN);
    moveDelay = (int)(ledSpeed / 1023 * (MAX_MOVE_DELAY - MIN_MOVE_DELAY)+MIN_MOVE_DELAY);
    
    analogWrite(PULSELED_PIN, currIntensity);   
    currIntensity = currIntensity + fadeAmount;
    if (currIntensity == 0 || currIntensity == 255) {
      fadeAmount = -fadeAmount ; 
    }     
    pressed = digitalRead(BUT01_PIN);
    if (!pressed){
      long sleepTime = (moveDelay - MIN_MOVE_DELAY)*20/(MAX_MOVE_DELAY - MIN_MOVE_DELAY) + 5;
      // delay(20);
      delay(sleepTime);
    } 
  }  
  analogWrite(PULSELED_PIN, 0);   
}

void flashLed(){
    analogWrite(PULSELED_PIN, 255);   
    delay(250);
    analogWrite(PULSELED_PIN, 0);     
}
