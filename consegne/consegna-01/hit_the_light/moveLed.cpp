#include "Arduino.h"
#include "config.h"
#include "moveLed.h"

extern int ledPin[3];
extern int selectedLed;

int moveDelay = (MAX_MOVE_DELAY - MIN_MOVE_DELAY)/2 + MIN_MOVE_DELAY;

void moveLed(){
  int currentLed = 0;
  long duration = random(MIN_MOVE_DURATION,MAX_MOVE_DURATION);
  long currentTime = 0;
  long t0 = millis();
  int step = 1;
  while (currentTime < duration){
    digitalWrite(ledPin[currentLed],HIGH);
    delay(50);
    digitalWrite(ledPin[currentLed],LOW);
    currentLed = (currentLed + step);
    if (currentLed > 2){
      currentLed = 1;
      step = -1;     
    } else if (currentLed < 0){
      currentLed = 1;
      step = 1; 
    }
    delay(moveDelay);
    long t1 = millis();
    currentTime = currentTime + (t1 - t0); 
    // Serial.println(currentTime);
    t0 = t1;
  }  
  selectedLed = currentLed;
}
