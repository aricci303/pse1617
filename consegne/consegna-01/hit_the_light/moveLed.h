/*
 * This module is responsible of led animation,
 * once started a game
 *
 */
#ifndef __MOVELED__
#define __MOVELED__

#define MAX_MOVE_DELAY 500
#define MIN_MOVE_DELAY 10

void moveLed();

#endif

