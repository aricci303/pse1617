/*
 * PSE a.a. 2016/2017
 *
 * CONSEGNA #1 - HIT THE LIGHT GAME
 *
 * author: A. Ricci
 *
 */
#include "config.h"
#include "moveLed.h"
#include "pulseLed.h"
#include "checkUserSelection.h"


/*
 * The following global variables are shared among the
 * modules that represent the individual activities
 * run by the embedded system
 */
 
/* list of the leds used */
int ledPin[] = {LED01_PIN, LED02_PIN, LED03_PIN};

/* list of the corresponding button */
int buttonPin[] = {BUT01_PIN, BUT02_PIN, BUT03_PIN};

int selectedLed;  // index of the led chosen each game 
long score; // player's score     
boolean gameover; // flag set when
long maxDelayUserSelection; // for each game: max delay allowed for the user selection

void setup() {
  for (int i = 0; i < 3; i++){
    pinMode(ledPin[i], OUTPUT);     
  } 
  for (int i = 0; i < 3; i++){
    pinMode(buttonPin[i], INPUT);     
  } 
  pinMode(PULSELED_PIN,OUTPUT);  
  Serial.begin(9600);
}

void loop(){
  Serial.println(".:: WELCOME to Hit the Light Game ::.");
  pulseLed();
  gameover = false;
  maxDelayUserSelection = MAX_INITIAL_DELAY_USER_SEL;
  score = 0;
  Serial.println("New game started!");
  while (!gameover){  
    moveLed(); 
    checkUserSelection();
    if (!gameover){
      flashLed();  
    }
  }
  Serial.println("Game over - Punteggio finale: "+String(score)+"\n");
  delay(250);
}
