#include "MsgService.h"

MsgService msgService;

void setup() {
  msgService.init();  
}

void loop() {
  if (msgService.isMsgAvailable()) {
    Msg* msg = msgService.receiveMsg();
    if (msg->getContent() == "ping"){
       msgService.sendMsg(Msg("pong"));
       delay(500);
    }
    delete msg;
  }
}

