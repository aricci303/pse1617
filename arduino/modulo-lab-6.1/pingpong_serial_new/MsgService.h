#ifndef __MSGSERVICE__
#define __MSGSERVICE__

#include "Arduino.h"

class Msg {
  String content;

public:
  Msg(const String& content){
    this->content = content;
  }
  
  String getContent(){
    return content;
  }
};

class MsgService {
    
public:   
  void init();  
  bool isMsgAvailable();
  Msg* receiveMsg();
  void sendMsg(Msg msg);

};

#endif

