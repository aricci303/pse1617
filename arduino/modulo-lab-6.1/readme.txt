README

- ping_pong_bt e basic_example_bt contengono due esempi per
  la comunicazione con Arduino via Bluetooth,
  usata a lezione e in lab per far comunicare Android con Arduino
   
- ping_pong_serial e basic_example_serial contengono gli stessi esempi 
  utilizzando la seriale HW, utile per la comunicazione fra
  emulatore Android e Arduino

- basic_example_serial_new e ping_pong_serial_new sono 
  una variante di basic_example_serial e 
  ping_pong_serial, in cui si usa una implementazione 
  diversa (più robusta) del MsgService. Usa un protocollo di
  scambio di messaggi via seriale classico, ove un messaggio è 
  codificato come sequenza di caratteri con terminatore new line ('\n').
  Si sfrutta la procedura serialEvent per leggere i caratteri 
  man mano che sono disponibili.
