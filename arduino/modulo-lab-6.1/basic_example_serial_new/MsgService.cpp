#include "Arduino.h"
#include "MsgService.h"

static Msg* currentMsg;
static bool msgAvailable;
static String content;

void MsgService::init(){
  content.reserve(256);
  Serial.begin(9600);
  while (!Serial){}
}

void MsgService::sendMsg(Msg msg){
  Serial.println(msg.getContent());  
}

bool MsgService::isMsgAvailable(){
  return msgAvailable;
}

Msg* MsgService::receiveMsg(){
  if (msgAvailable){
    Msg* msg = currentMsg;
    msgAvailable = false;
    currentMsg = NULL;
    content = "";
    return msg;  
  } else {
    return NULL; 
  }
}

void serialEvent() {
  /* reading the content */
  while (Serial.available()) {
    char ch = (char) Serial.read();
    if (ch == '\n'){
      currentMsg = new Msg(content);
      msgAvailable = true;      
    } else {
      content += ch;      
    }
  }
}




