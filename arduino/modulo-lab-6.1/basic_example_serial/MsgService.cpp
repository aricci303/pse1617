#include "Arduino.h"
#include "MsgService.h"


void MsgService::init(){
  content.reserve(256);
  Serial.begin(9600);
  while (!Serial){}
}

bool MsgService::sendMsg(Msg msg){
  byte output[256];
  String content = msg.getContent();
  int len = content.length();
  if (len >= 0 && len <= 255){
    output[0] = (byte) len;
    content.getBytes((output+1),len+1);
    Serial.write((const char*)output,len+1);
    return true;
  } else {
    return false;
  }
}

bool MsgService::isMsgAvailable(){
  return Serial.available();
}

Msg* MsgService::receiveMsg(){
  if (Serial.available()){    
    content="";
    int size = Serial.read();
    // Serial.println("> "+String(size));
    int nDataRec = 0;
    while (nDataRec < size) {
      if (Serial.available()){
        content += (char)Serial.read();      
        nDataRec++;
      }
    }
    return new Msg(content);
  } else {
    return NULL;  
  }
}




