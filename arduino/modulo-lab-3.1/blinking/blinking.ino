#include "Led.h"

#define LED_PIN 13

Light* led;

void setup(){
  led = new Led(LED_PIN);  
}

void loop(){
  led->switchOn();
  delay(100);
  led->switchOff(); 
  delay(100);
};
