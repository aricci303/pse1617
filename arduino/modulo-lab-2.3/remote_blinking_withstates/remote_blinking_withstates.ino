#include "Led.h"
#include "Timer.h"
#include "MsgService.h"

#define LED_PIN 13

Light* led;
Timer timer;

enum { ON, OFF, IDLE} state;

void setup(){
  MsgService.init();
  led = new Led(LED_PIN); 
  state = IDLE;
  timer.setupPeriod(500);
}

void step(){
  switch (state){
    case IDLE:
      if (MsgService.isMsgAvailable()){
        Msg* msg = MsgService.receiveMsg();  
        if (msg->getContent()=="start"){
          MsgService.sendMsg("started");
          state = ON;  
        }
        delete msg;        
      }
      break;
    case OFF:   
      if (MsgService.isMsgAvailable()){
        Msg* msg = MsgService.receiveMsg();  
        if (msg->getContent()=="stop"){
          state = IDLE;  
          MsgService.sendMsg("stopped");
        }
        delete msg;        
      } else {
        led->switchOn();
        state = ON;
      }
      break;
    case ON:
      if (MsgService.isMsgAvailable()){
        Msg* msg = MsgService.receiveMsg();  
        if (msg->getContent()=="stop"){
          state = IDLE;  
          MsgService.sendMsg("stopped");
        }
        delete msg;        
      } else {
        led->switchOff();
        state = OFF;
      }
  }
}

void loop(){
  timer.waitForNextTick();
  step();
};
