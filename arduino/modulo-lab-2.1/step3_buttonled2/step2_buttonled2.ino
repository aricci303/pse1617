//
#define BUTTON_PIN 2

// deve essere uno che supporta PWM
#define LED_PIN 9

int fadeAmount;
int currIntensity;


void setup(){
  currIntensity = 0;
  fadeAmount = 5;
  pinMode(LED_PIN, OUTPUT);     
  pinMode(BUTTON_PIN, INPUT);
  Serial.begin(9600);
}

void loop(){
  int buttonState = digitalRead(BUTTON_PIN);
  if (buttonState == HIGH){ 
    Serial.println("1");
    analogWrite(LED_PIN, currIntensity);   
    currIntensity = currIntensity + fadeAmount;
    if (currIntensity == 0 || currIntensity == 255) {
      fadeAmount = -fadeAmount ; 
    }     
    delay(20);                              
  } else {
    // reset
    Serial.println("0");
    currIntensity = 0;
    analogWrite(LED_PIN, currIntensity);   
  }
}
