#include "TimerOne.h"

const int LED_PIN = 13;
boolean flagState = false;

void blinky(){
  if (!flagState){
    digitalWrite(LED_PIN, HIGH);  
  } else {
    digitalWrite(LED_PIN, LOW);  
  }
  flagState = !flagState;
}

void setup()
{
  pinMode(LED_PIN,OUTPUT);
  Timer1.initialize(100000); // set a timer of length 10^6 micro sec = 1 sec
  Timer1.attachInterrupt(blinky); // runs blink on each timer interrupt
}

void loop(){}

