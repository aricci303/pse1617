#define PIR_PIN 7

void setup(){
  pinMode(PIR_PIN,INPUT);
  Serial.begin(9600);
}

void loop(){
  int detected = digitalRead(PIR_PIN);
  if (detected == HIGH){
    Serial.println("detected!");  
  }
};
