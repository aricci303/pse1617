int servoPin = 2;

void pulseServo(int servoPin, int pulseLen)
{
  digitalWrite(servoPin, HIGH);
  delayMicroseconds(pulseLen);  
  digitalWrite(servoPin,LOW);
  delay(15);
}

void setup()
{
   pinMode(servoPin, OUTPUT);
   Serial.begin(9600);
}

void loop()
{
  for (int i = 250; i<=3000; i++){
    pulseServo(servoPin, i);
    Serial.println(i);
  }
}
