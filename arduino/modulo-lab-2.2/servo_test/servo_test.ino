int servoPin = 2;

void pulseServo(int servoPin, int pulseLen)
{
  digitalWrite(servoPin, HIGH);
  delayMicroseconds(pulseLen);  
  digitalWrite(servoPin,LOW);
  delay(20);
}

int c;
enum { MINUS_90, PLUS_90 } state;

void setup()
{
   pinMode(servoPin, OUTPUT);
   state = MINUS_90;
   Serial.begin(9600);
   c = 0;
}

void loop(){
  c++;
  switch (state) {
    case MINUS_90: 
      pulseServo(servoPin, 1000);
      if (c > 100){
        Serial.println("--> +90");
        state = PLUS_90;
        c = 0;
      }
      break;
    case PLUS_90: 
      pulseServo(servoPin, 2000);
      if (c > 100){
        Serial.println("--> -90");
        state = MINUS_90;
        c = 0;
      }
  }
}
