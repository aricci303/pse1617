#include "Sonar.h"

const int trigPin = 8;
const int echoPin = 7;

ProximitySensor* proxSensor;

void setup()
{
  Serial.begin(9600);
  proxSensor = new Sonar(echoPin,trigPin);
}

void loop()
{
  float d = proxSensor->getDistance();
  Serial.println(d);
  delay(200); 
}

