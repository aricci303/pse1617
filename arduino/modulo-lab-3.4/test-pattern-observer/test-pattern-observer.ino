#include "ButtonImpl.h"
 
class MyListener : public ButtonListener {
public:
  MyListener(){
    count = 0;  
  }
  
  void notifyButtonPressed(){
    count++;
    while (1){}
  }
  
  int getCount(){
    noInterrupts();
    int c = count;
    interrupts();
    return c;  
  }  
private:
  volatile int count;
};

Button* button;
MyListener* listener;

void setup(){
  Serial.begin(9600);
  button = new ButtonImpl(2);
  listener = new MyListener();
  button->registerListener(listener);
}

void loop(){
  Serial.println(listener->getCount());
};
