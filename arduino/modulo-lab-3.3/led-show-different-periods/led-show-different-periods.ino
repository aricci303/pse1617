#include "Timer.h"
#include "BlinkTask.h"
#include "ThreeLedsTask.h"

const int basePeriod = 50;

Timer timer;

BlinkTask blinkTask(2);
ThreeLedsTask threeLedsTask(3,4,5);

void setup(){
  timer.setupPeriod(basePeriod);
  blinkTask.init(500);
  threeLedsTask.init(150);
}

void loop(){
  timer.waitForNextTick();
  if (blinkTask.updateAndCheckTime(basePeriod)){
    blinkTask.tick();
  }
  if (threeLedsTask.updateAndCheckTime(basePeriod)){
    threeLedsTask.tick();
  }
}
