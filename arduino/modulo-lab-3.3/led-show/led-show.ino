#include "Timer.h"
#include "BlinkTask.h"
#include "ThreeLedsTask.h"

Timer timer;

BlinkTask blinkTask(2);
ThreeLedsTask threeLedsTask(3,4,5);

void setup(){
  blinkTask.init();
  threeLedsTask.init();
  timer.setupPeriod(500);
}

void loop(){
  timer.waitForNextTick();
  blinkTask.tick();
  threeLedsTask.tick();
};
