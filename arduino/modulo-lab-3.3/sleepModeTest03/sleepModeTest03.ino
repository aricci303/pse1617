#include <avr/sleep.h>

volatile unsigned long count = 0;

ISR(TIMER2_COMPA_vect){
  count+=10;
}

void wakeUp(){}

void initTimer2(){
  cli();//stop interrupts
  //set timer2 interrupt at 100 Hz - prescaler 1024
  TCCR2A = 0;// set entire TCCR2A register to 0
  TCCR2B = 0;// same for TCCR2B
  TCNT2  = 0;//initialize counter value to 0
  // set compare match register for 100 Hz increments
  OCR2A = 159;// = (16*10^6) / (freq*prescaler) - 1 (must be <256)
  // OCR2A = 159;// = (16*10^6) / (100*1024) - 1 (must be <256)
  // turn on CTC mode
  TCCR2A |= (1 << WGM21);
  // Set CS22+CS20 bit for 1024 prescaler
  TCCR2B |= (1 << CS22) | (1 << CS20);
  // enable timer compare interrupt
  TIMSK2 |= (1 << OCIE2A);

  sei();//allow interrupts
  
}
void setup(){  
  Serial.begin(9600);  
  pinMode(2,INPUT);
  attachInterrupt(digitalPinToInterrupt(2), wakeUp, RISING); 
  initTimer2();
}

void loop(){
  set_sleep_mode(SLEEP_MODE_PWR_SAVE);  
  sleep_enable();
  sleep_mode();  
  if (count % 10000 == 0){
    Serial.println("WAKE UP "+String(count));
    delay(100);
  }
  /** The program will continue from here. **/  
  /* First thing to do is disable sleep. */  
  sleep_disable();   
}
