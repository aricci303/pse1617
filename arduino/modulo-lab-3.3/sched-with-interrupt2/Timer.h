#ifndef __TIMER__
#define __TIMER__
    
#include "Runnable.h"

class Timer {
public:  
  Timer(Runnable* task);
  void setupFreq(int freq);  
  /* period in ms */
  void setupPeriod(int period);  
  void waitForNextTick();

};


#endif

