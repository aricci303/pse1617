#include "Scheduler.h"
#include "BlinkTask.h"

Scheduler sched;

void setup(){
  
  sched.init(1000);
  
  Task* t0 = new BlinkTask(3);
  t0->init(2000);
  sched.addTask(t0);
   
}

void loop(){
  sched.run();
}
