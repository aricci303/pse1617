#ifndef __TIMER__
#define __TIMER__
    
class Scheduler;

class Timer {
public:  
  Timer(Scheduler* pSched);
  void setupFreq(int freq);  
  /* period in ms */
  void setupPeriod(int period);  
  void waitForNextTick();

};


#endif

