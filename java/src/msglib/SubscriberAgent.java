package msglib;

import msglib.*;

public abstract class SubscriberAgent extends BaseAgent {

	public SubscriberAgent(String name, int port) throws Exception {
		super(name,port);
	}

	protected void subscribe(String brokerAddress, int port, String topic) throws Exception {
		sendMsg(brokerAddress,port,"subscribe$"+topic+"$"+getFullAddress());
	}
}