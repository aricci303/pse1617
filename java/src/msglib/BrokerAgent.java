package msglib;

import java.util.*;

public class BrokerAgent extends BaseAgent {

	private HashMap<String,List<AgentAddress>> topics;
	
	public BrokerAgent(int port) throws Exception {
		super("msg-broker", port);
		topics = new HashMap<String,List<AgentAddress>>();
	}
	
	protected void setup() throws Exception {
		log("Broker Ready on "+getFullAddress());
	}

	protected void loop() throws Exception {
			String msg = waitForMsg();
			log("New msg: "+msg);
			if (msg.startsWith("subscribe$")){
				msg = msg.substring(10);
				int index = msg.indexOf("$");
				if (index != -1){
					String topic = msg.substring(0,index);
					String address = msg.substring(index+1);
					log("Subscribing "+address+" to "+topic);
					List<AgentAddress> addresses = topics.get(topic);
					if (addresses == null){
						addresses = new LinkedList<AgentAddress>();
						topics.put(topic, addresses);
					}
					addresses.add(new AgentAddress(address));
				}
			} else {
				int index = msg.indexOf("$");
				if (index != -1){
					String topic = msg.substring(0,index);
					String content = msg.substring(index+1);
					log("Publishing "+content+" on topic: "+topic);
					List<AgentAddress> addresses = topics.get(topic);
					if (addresses != null){
						for (AgentAddress addr: addresses){
							try {
								this.sendMsg(addr.getAddress(), addr.getPort(), content);
							} catch (Exception ex){}
						}
					}
				}
				
			}
	}
	
	static class AgentAddress {

		String address;
		int port;
		
		AgentAddress(String addr) throws Exception {
			int index = addr.indexOf(':');
			address = addr.substring(0, index);
			port = Integer.parseInt(addr.substring(index+1));
		}
		
		public String getAddress(){
			return address;
		}
		
		public int getPort(){
			return port;
		}
		
		public String toString(){
			return address+":"+port;
		}
	}
	
}