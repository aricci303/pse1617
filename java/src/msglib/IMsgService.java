package msglib;


/**
 * Interface for simple messaging services.
 * 
 * @author aricci
 *
 */
public interface IMsgService {


	/**
	 * Send a message to a specified agent
	 * 
	 * @param dest target of the message
	 * @param msg message to send
	 */
	void sendMsg(String dest, int port, String msg) throws Exception;
	
	/**
	 * Receive a message.
	 * 
	 * Blocking behaviour.
	 * 
	 * @return message received
	 * @throws Exception
	 */
	Msg receiveMsg() throws Exception;
	
	
}