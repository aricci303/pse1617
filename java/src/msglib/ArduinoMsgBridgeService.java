package msglib;

import gnu.io.CommPortIdentifier;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.Enumeration;
import java.util.Random;

class MsgReceiver extends Thread {
	
	private MsgServiceUDP socket;
	private SerialCommChannel channel;
	
	public MsgReceiver(MsgServiceUDP socket, SerialCommChannel channel){
		this.channel = channel;
		this.socket = socket;
	}
	
	public void run(){
			int port = 20001;
			while (true){
				try {
					String msg = channel.receiveMsg();
					// check if a dest is specified
					log("[BRIDGE] new msg from Arduino: "+msg);
					int index = msg.indexOf(":::");
					if (index != -1){
						String target = msg.substring(0,index);
						int indexPort = target.indexOf(":");
						if (indexPort!=-1){
							port = Integer.parseInt(target.substring(indexPort+1));
							target = target.substring(0, indexPort);
						}
						msg = msg.substring(index+3);
						log("[BRIDGE] forwarding msg to "+target+":"+port+" - content: "+msg);
						try {
							socket.sendMsg(target,port,msg);
						} catch (Exception ex){
							log("[BRIDGE] forwarding failed. ");
						}
					}
				} catch (Exception ex){
					ex.printStackTrace();
					log("[BRIDGE] MsgReceiver Exception - recovered.");
				}
			}
	}
	
	private void log(String msg){
		System.out.println(msg);
	}
	
}

class MsgForwarder extends Thread {
	
	private MsgServiceUDP socket;
	private SerialCommChannel channel;
	
	public MsgForwarder(MsgServiceUDP socket, SerialCommChannel channel){
		this.channel = channel;
		this.socket = socket;
	}
	
	public void run(){
			while (true){
				try {
					Msg msg = socket.receiveMsg();
					log("[BRIDGE] new msg to Arduino :"+msg.getContent());
					channel.sendMsg(msg.getContent());
				} catch (Exception ex){
					ex.printStackTrace();
					log("[BRIDGE] MsgForwarder Exception - recovered.");
				}
			}
	}
	
	private void log(String msg){
		System.out.println(msg);
	}
}

public class ArduinoMsgBridgeService {

	public static void main(String[] args) throws Exception {
		
		if (args.length == 0){
			System.out.println("args: <CommPortName> <LocalPort>");
			System.out.println("Available serial ports:");
			Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
			while (portEnum.hasMoreElements()) {
				CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
				System.out.println(currPortId.getName());
			}
		} else {			
			SerialCommChannel channel = new SerialCommChannel(args[0],115200);			
			int port = Integer.parseInt(args[1]);
			MsgServiceUDP socket = new MsgServiceUDP(port);					
			new MsgReceiver(socket, channel).start();
			new MsgForwarder(socket, channel).start();	
			System.out.println("Ready.");

		}
		
	}

}
