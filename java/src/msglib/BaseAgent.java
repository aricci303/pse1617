package msglib;

import msglib.*;

public abstract class BaseAgent extends Thread {
	
	private MsgServiceUDP service;
	private String name;
	
	protected BaseAgent(String name, int port) throws Exception{
		super(name);
		service = new MsgServiceUDP(port);
	}
	
	public void run(){
		try {
			setup();
			while (true){
				loop();
			}
		} catch (Exception ex){
			ex.printStackTrace();
		}
		
	}
	
	protected void setup() throws Exception{}
	protected void loop() throws Exception {}

	final protected void sendMsg(String dest, int port, String msg) throws Exception {
		service.sendMsg(dest, port, msg);
	}
	
	final protected String waitForMsg() throws Exception {
		return service.receiveMsg().getContent();
	}
	
	final protected void log(String msg){
		System.out.println("[AGENT "+getName()+"] "+msg);
	}
	
	final protected String getFullAddress() {
		return service.getFullAddress();
	}
}
