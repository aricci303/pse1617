package msglib;

public class Msg {

	private String content;
	
	public Msg(String content){
		this.content = content;
	}
	
	public String getContent(){
		return content;
	}
}
