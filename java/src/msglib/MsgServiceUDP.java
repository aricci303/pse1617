package msglib;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class MsgServiceUDP implements IMsgService {

	private DatagramSocket socket;
	private InetAddress group;
	private byte[] buf;
	private String owner;
	private String udpAddress;
		
	/**
	 * Install the message service on the default address.
	 * 
	 * @throws Exception
	 */
	public MsgServiceUDP(int port) throws Exception {
		socket = new DatagramSocket(port);
        buf = new byte[1000];
	}

	
	/**
	 * Send a message
	 * 
	 * @param msg message to send
	 */
	public void sendMsg(String dest, int port, String msg) throws Exception {
	   InetAddress address = InetAddress.getByName(dest);
       DatagramPacket dp = new DatagramPacket(msg.getBytes(), msg.length(), address, port);
       socket.send(dp);				
	}	

	
	/**
	 * Receive a message - blocking.
	 */
	public Msg receiveMsg() throws Exception {
		while (true) {
	        DatagramPacket recv = new DatagramPacket(buf, buf.length);
			socket.receive(recv);
		    String content =  new String(recv.getData(),0,recv.getLength());
		    return new Msg(content);
		}
	}
	
	public String getFullAddress() {
		try {
			InetAddress localHost = InetAddress.getLocalHost();
			return localHost.getHostAddress()+":"+socket.getLocalPort();
		} catch (Exception ex){
			return null;
		}
	}

}