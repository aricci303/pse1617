package pse.consegna02;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MonitoringAgent extends Thread {

	SerialCommChannel channel;
	SmartToiletView view;
	
	public MonitoringAgent(SerialCommChannel channel, SmartToiletView view) throws Exception {
		this.view = view;
		this.channel = channel;
	}
	
	public void run(){
		while (true){
			try {
				String msg = channel.receiveMsg();
				if (msg.startsWith("display:")){
					view.displayMsg(msg.substring(8));
				} else if (msg.startsWith("req_state:")) {
					view.setState(msg.substring(10));
				} else if (msg.startsWith("req_nusers:")) {
					view.setNUsers(msg.substring(11));
				} else if (msg.startsWith("req_nusages:")) {
					view.setNUsages(msg.substring(12));
				}
			} catch (Exception ex){
				ex.printStackTrace();
			}
		}
	}

}
