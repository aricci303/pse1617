package pse.consegna02;

class SmartToiletMain   {

	public static void main(String[] args) throws Exception {
		SmartToiletView view = new SmartToiletView();
		Controller contr = new Controller(args[0],view);
		view.registerController(contr);
		view.setVisible(true);

	}
}