package pse.consegna02;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller implements ActionListener {

	SerialCommChannel channel;
	SmartToiletView view;
	
	public Controller(String port, SmartToiletView view) throws Exception {
		this.view = view;
		channel = new SerialCommChannel(port,9600);		
		new MonitoringAgent(channel,view).start();
			
		/* attesa necessaria per fare in modo che Arduino completi il reboot */
		System.out.println("Waiting Arduino for rebooting...");		
		Thread.sleep(4000);
		System.out.println("Ready.");		
	
	}
	
	public void actionPerformed(ActionEvent ev){
		  try {
			  if (ev.getActionCommand().equals("Get State")){
					channel.sendMsg("req_state");
			  } else if (ev.getActionCommand().equals("Get Num Users")){
					channel.sendMsg("req_nusers");
			  } else if (ev.getActionCommand().equals("Get Num Usages")){
					channel.sendMsg("req_nusages");
			  } 
		  } catch (Exception ex){
			  ex.printStackTrace();
		  }
	  }

}
