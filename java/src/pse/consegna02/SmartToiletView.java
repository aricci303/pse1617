package pse.consegna02;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

class SmartToiletView extends JFrame  {

	private JTextField state;
	private JButton buttonState;

	private JTextField nUsers;
	private JButton buttonGetUsers;

	private JTextField nUsages;
	private JButton buttonGetUsages;

	private JTextField messages;

	  public SmartToiletView(){
	    super("Smart Toilet View");
	    setSize(400,120);
	    
	    JPanel mainPanel = new JPanel();
	    mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
	    
	    JPanel reqMsgPanel = new JPanel();
	    reqMsgPanel.setLayout(new BoxLayout(reqMsgPanel, BoxLayout.LINE_AXIS));
	    reqMsgPanel.add(new JLabel("Messages: "));
	    messages = new JTextField(60);
	    reqMsgPanel.add(messages);
	    mainPanel.add(reqMsgPanel);

	    JPanel reqStatePanel = new JPanel();
	    reqStatePanel.setLayout(new BoxLayout(reqStatePanel, BoxLayout.LINE_AXIS));
	    buttonState = new JButton("Get State");
	    reqStatePanel.add(buttonState);
	    state = new JTextField(40);
	    reqStatePanel.add(state);
	    mainPanel.add(reqStatePanel);

	    JPanel reqNUsersPanel = new JPanel();
	    reqNUsersPanel.setLayout(new BoxLayout(reqNUsersPanel, BoxLayout.LINE_AXIS));
	    buttonGetUsers = new JButton("Get Num Users");
	    reqNUsersPanel.add(buttonGetUsers);
	    nUsers = new JTextField(40);
	    reqNUsersPanel.add(nUsers);
	    mainPanel.add(reqNUsersPanel);
	    
	    JPanel reqNUsagesPanel = new JPanel();
	    reqNUsagesPanel.setLayout(new BoxLayout(reqNUsagesPanel, BoxLayout.LINE_AXIS));
	    buttonGetUsages = new JButton("Get Num Usages");
	    reqNUsagesPanel.add(buttonGetUsages);
	    nUsages = new JTextField(40);
	    reqNUsagesPanel.add(nUsages);
	    mainPanel.add(reqNUsagesPanel);

	    this.getContentPane().add(mainPanel);
	    
	    addWindowListener(new WindowAdapter(){
	      public void windowClosing(WindowEvent ev){
	        System.exit(-1);
	      }
	    });
	  }

	  public void registerController(Controller contr){
		  buttonState.addActionListener(contr);
		  buttonGetUsers.addActionListener(contr);
		  buttonGetUsages.addActionListener(contr);
	  }
	  
	  public void setState(String msg){
		  SwingUtilities.invokeLater(() -> {
			 state.setText(msg);
		  });
	  }

	  public void setNUsers(String msg){
		  SwingUtilities.invokeLater(() -> {
			 nUsers.setText(msg);
		  });
	  }

	  public void setNUsages(String msg){
		  SwingUtilities.invokeLater(() -> {
			 nUsages.setText(msg);
		  });
	  }
	  
	  public void displayMsg(String msg){
		  SwingUtilities.invokeLater(() -> {
			 messages.setText(msg);
		  });
	  }
	}
