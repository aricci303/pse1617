package pse.modulo_lab_4_2.common;

public interface Observer {

	boolean notifyEvent(Event ev);
}
