package pse.modulo_lab_4_2.devices;

public interface ProximitySensor {

	boolean isObjDetected();
	
	double getObjDistance();
	
}
