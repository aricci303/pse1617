package pse.modulo_lab_4_2.devices;

public interface Button {
	
	boolean isPressed();

}
