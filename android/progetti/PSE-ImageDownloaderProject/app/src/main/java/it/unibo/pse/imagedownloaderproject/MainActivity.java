package it.unibo.pse.imagedownloaderproject;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends Activity {

    private Button downloadButton;
    private EditText urlField;
    private ImageView imageContainer;

    private static final String DEFAULT_IMAGE_URL = "http://www.informatica.unibo.it/it/risorse/immagini/sede-di-cesena/@@images/506f1064-a7b7-40a0-ac54-ed2bc1714b13.jpeg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();
    }

    private void initUI() {
        urlField = (EditText) findViewById(R.id.urlEditText);
        urlField.setText(DEFAULT_IMAGE_URL);

        imageContainer = (ImageView) findViewById(R.id.imageView);

        downloadButton = (Button) findViewById(R.id.button);
        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            final URL url = new URL(urlField.getText().toString());
                            final Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    imageContainer.setImageBitmap(bmp);
                                }
                            });

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });

                thread.start();
            }
        });
    }
}

