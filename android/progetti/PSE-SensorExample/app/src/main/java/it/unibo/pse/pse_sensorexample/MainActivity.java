package it.unibo.pse.pse_sensorexample;

import android.Manifest;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final int ACCESS_FINE_LOCATION_REQUEST = 1234;

    private SensorManager sm;
    private Sensor accelerometer;
    private SensorEventListener accelerometerListener;

    private LocationManager lm;
    private LocationListener locationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        if (accelerometer != null)
            accelerometerListener = new MyAccelerometerListener();

        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = new MyLocationListener();
    }

    @Override
    protected void onStart() {
        super.onStart();

        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (permission == PackageManager.PERMISSION_GRANTED) {
            requestLocationUpdates();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_REQUEST);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(accelerometer != null)
            sm.registerListener(accelerometerListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(accelerometer != null)
            sm.unregisterListener(accelerometerListener);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ACCESS_FINE_LOCATION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestLocationUpdates();
                } else {
                    Log.d("PSE-APP", "Permission denied!");
                }
                break;
        }
    }

    private void requestLocationUpdates() throws SecurityException {
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }

    class MyAccelerometerListener implements SensorEventListener{
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {

            TextView xVal = (TextView) findViewById(R.id.accXval);
            TextView yVal = (TextView) findViewById(R.id.accYval);
            TextView zVal = (TextView) findViewById(R.id.accZval);

            xVal.setText("X = " + sensorEvent.values[0]);
            yVal.setText("Y = " + sensorEvent.values[1]);
            zVal.setText("Z = " + sensorEvent.values[2]);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    }

    class MyLocationListener implements LocationListener{
        public void onLocationChanged(Location location) {
            TextView locationLabel = (TextView) findViewById(R.id.actualLocationTextView);
            locationLabel.setText("(" + location.getLatitude() + "," + location.getLongitude() + ")");
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        public void onProviderEnabled(String provider) {

        }

        public void onProviderDisabled(String provider) {

        }
    }
}
