package connection.lib;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.net.InetAddress;
import java.net.Socket;

public class TCPClient extends Thread {

    public static final String TAG = "TCPClient";

    public final String SERVER_IP;
    public final int SERVER_PORT;


    private boolean mRun = false;
    private OutputStream mBufferOut;
    private InputStream mBufferIn;

    private Handler handler;

    public TCPClient(String serverIP, int serverPort, Handler handler) {
        this.handler = handler;
        this.SERVER_IP = serverIP;
        this.SERVER_PORT = serverPort;
    }

    public void run() {
        mRun = true;

        try {
            Socket socket = new Socket(InetAddress.getByName(SERVER_IP), SERVER_PORT);

            try{
                mBufferOut = socket.getOutputStream();
                mBufferIn = socket.getInputStream();

                while (mRun) {
                    try {
                        int size = mBufferIn.read();

                        int i = 0;
                        StringBuffer buf = new StringBuffer("");

                        while (i < size) {
                            int data = mBufferIn.read();
                            buf.append((char) data);
                            i++;
                        }

                        Message m = new Message();
                        m.obj = buf.toString();
                        handler.sendMessage(m);

                    } catch (Exception e) {
                        System.err.println(e.toString());
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "S: Error", e);
            } finally {
                socket.close();
            }
        } catch (Exception e) {
            Log.e(TAG, "C: Error", e);
        }
    }

    public void sendMessage(String message) throws MsgTooBigException {
        if (message.length() > 255) {
            throw new MsgTooBigException();
        }

        char[] array = message.toCharArray();

        byte[] bytes = new byte[array.length + 1];
        bytes[0] = (byte) message.length();

        for (int i = 0; i < array.length; i++) {
            bytes[i + 1] = (byte) array[i];
        }

        try {
            mBufferOut.write(bytes);
            mBufferOut.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void stopClient() {
        mRun = false;

        if (mBufferOut != null) {
            try {
                mBufferOut.flush();
                mBufferOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        mBufferIn = null;
        mBufferOut = null;
    }
}
