package it.unibo.pse;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import connection.lib.MsgTooBigException;
import connection.lib.TCPClient;

import it.unibo.pse.testcomportsexample.R;

public class MainActivity extends Activity {

    private static final String TAG = "PSE-APP";

    private static final String HOST_IP_FROM_EMULATOR = "10.0.2.2";
    private static final int HOST_PORT = 8080;

    private TCPClient tcpClient;

    private static MainActivityHandler uiHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();

        uiHandler = new MainActivityHandler(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        tcpClient = new TCPClient(HOST_IP_FROM_EMULATOR, HOST_PORT, uiHandler);
        tcpClient.start();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(tcpClient != null) {
            Log.d(TAG, "stopClient");
            tcpClient.stopClient();
            tcpClient = null;
        }
    }

    private void initUI() {
        Button sendButton = (Button) findViewById(R.id.buttonSend);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String msg = ((EditText) findViewById(R.id.editText)).getText().toString();
                    tcpClient.sendMessage(msg);
                } catch (MsgTooBigException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Handler
     */
    public static class MainActivityHandler extends Handler {
        private final WeakReference<MainActivity> context;

        MainActivityHandler(MainActivity context){
            this.context = new WeakReference<>(context);
        }

        public void handleMessage(Message msg) {
            Object obj = msg.obj;

            if(obj instanceof String){
                String message = obj.toString();
                ((TextView) context.get().findViewById(R.id.textView)).append("\n > " + message);
            }
        }
    }
}
